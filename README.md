# UNOS ML Prediction

## Installation

```bash
pipenv shell
pipenv install --dev
jupyter notebook
```

## How to

In `notebooks/heart_transplant/heart_transplant_evaluate.ipynb` are the output data.

The input data must be place in the adjacent directory `cardiovascular-risk-data`.

File `heart_transplant_metadata.py` contains the human-readable names of the used features.

